export interface Subject extends SubjectUpdate {
  creatorId: string;
  createdAt: Date;
}

export interface SubjectCreate {
  title: string;
  description: string;
  groupId: string;
  creatorId?: string;
}

export interface SubjectUpdate extends SubjectCreate {
  id: string;
}

export interface UserSession {
  id: string;
  userId: string;
  username: string;
  fullName: string;
  validUntil: Date;
}

export interface Permission {
  id: string;
  name: string;
}
