import { FastifyPluginCallback, RouteShorthandOptions } from 'fastify';
import { findSession, getPermissions } from '../grpc/grpcClient';
import { Interceptor } from '../interceptor/interceptor';
import { ISubjectService } from '../services/subjectService';
import { SubjectCreate, SubjectUpdate } from '../types/subject';

export interface ISubjectPluginOptions {
  subjectService: ISubjectService;
  interceptor: Interceptor;
}

const createSubjectOptions: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['title', 'description', 'groupId'],
      properties: {
        title: { type: 'string' },
        description: { type: 'string' },
        groupId: { type: 'string' },
      },
    },
  },
};

const updateSubjectOptions: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['id', 'title', 'description'],
      properties: {
        id: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
      },
    },
  },
};

export const subjectPlugin: FastifyPluginCallback<ISubjectPluginOptions> = (fastify, options, done) => {
  fastify.post('/subject', createSubjectOptions, async (request, response) => {
    const subjectCreate = request.body as SubjectCreate;
    try {
      const session = await findSession(request.cookies['Session']);
      console.log('session finded');
      const permissions = (await getPermissions(subjectCreate.groupId, session.userId)).map((p) => p.name);
      console.log('permissions here');
      subjectCreate.creatorId = session.userId;
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'createSubject', permissions, () => options.subjectService.create(subjectCreate)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.put('/subject', updateSubjectOptions, async (request, response) => {
    const subjectUpdate = request.body as SubjectUpdate;
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await getPermissions(subjectUpdate.groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'updateSubject', permissions, () => options.subjectService.update(subjectUpdate)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.delete('/subject/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const subject = await options.subjectService.getById(id);
      const permissions = (await getPermissions(subject.groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'deleteSubject', permissions, () => options.subjectService.delete(id)
      );
      response.code(204);
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/subject/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const subject = await options.subjectService.getById(id);
      const permissions = (await getPermissions(subject.groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readSubject', permissions, () => options.subjectService.getById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/subject/all/:groupId', async (request, response) => {
    const { groupId } = request.params as { groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readSubject', permissions, () => options.subjectService.getSubjectsByGroupId(groupId)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  done();
};
