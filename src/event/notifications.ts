import { publishNotification } from './pubsub';
import { Subject } from '../types/subject';

export const sendSubjectCreatedNotification = (subject: Subject, user: any) => {
  publishNotification({
    data: { id: subject.id, title: subject.title, description: subject.description, groupId: subject.groupId },
    senderId: user.userId,
    type: 'SUBJECT_CREATED',
    message: `User ${user.fullName} has created subject ${subject.title}`,
    link: `/subject/${subject.id}`,
  });
};
