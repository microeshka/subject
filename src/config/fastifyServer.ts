import fastify, { FastifyReply, FastifyRequest, HookHandlerDoneFunction } from 'fastify';
import cors from 'fastify-cors';
import { healthCheckPlugin } from '../plugins/healthCheck';
import fastifyCookie from 'fastify-cookie';
import { RouteGenericInterface } from 'fastify/types/route';
import { IncomingMessage, ServerResponse } from 'http';
import { Server } from 'http';
import { ISubjectService } from '../services/subjectService';
import { ISubjectPluginOptions, subjectPlugin } from '../plugins/subject';
import { Interceptor } from '../interceptor/interceptor';

const allowUnauthorized = new Set(['/', '/healthz', '/subject/health']);

const cookieHook = (
    request: FastifyRequest<RouteGenericInterface, Server, IncomingMessage>,
    reply: FastifyReply<Server, IncomingMessage, ServerResponse, RouteGenericInterface, unknown>,
    done: HookHandlerDoneFunction
) => {
  if (!allowUnauthorized.has(request.url) && !request.cookies['Session']) {
    reply.code(401).send('User is not authorized');
  } else {
    done();
  }
};

export const createFastifyServer = (subjectService: ISubjectService, interceptor: Interceptor) => {
  const server = fastify({});
  server.register(cors, {
    origin: (_origin, cb) => {
      cb(null, true);
    },
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    credentials: true,
  });
  server.register(fastifyCookie);
  server.register(healthCheckPlugin, {});
  server.register(subjectPlugin, { subjectService, interceptor } as ISubjectPluginOptions);
  server.addHook('onRequest', cookieHook);
  return server;
};
