
export const permissionsConfig = {
  'createSubject': 'perm.subject.own.edit',
  'updateSubject': 'perm.subject.own.edit',
  'readSubject': 'perm.subject.own.edit.read',
  'deleteSubject': 'perm.subject.own.edit',
} as { [key: string]: string };
