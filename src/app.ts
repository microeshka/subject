import { PoolConfig } from 'pg';
import { createFastifyServer } from './config/fastifyServer';
import { SubjectRepository } from './db/subjectRepository';
import { PermissionChecker } from './interceptor/interceptor';
import { ISubjectService, SubjectService } from './services/subjectService';
import { checkAndGetEnv } from './utils';

export class Application {
  private subjectService: ISubjectService;
  private fastifyServer: any;

  constructor() {
    const dbConfig = {
      max: 20,
      idleTimeoutMillis: 10000,
      connectionTimeoutMillis: 2000,
      connectionString: checkAndGetEnv('MAIN_DB_CONNECTION_STRING'),
    } as PoolConfig;
    const subjectRepository = new SubjectRepository(dbConfig);
    this.subjectService = new SubjectService(subjectRepository);
    this.fastifyServer = createFastifyServer(this.subjectService, new PermissionChecker());
  }

  public startFastifyServer() {
    const port = checkAndGetEnv('HTTP_SERVER_PORT');
    this.fastifyServer.listen({ port }).then(() =>
      console.log(`App started! PORT: ${port}`),
    );
  }
}
