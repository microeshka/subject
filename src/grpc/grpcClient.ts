import { AuthClient } from '../proto/auth_grpc_pb';
import * as grpc from 'grpc';
import { FindSessionRequest, Session } from '../proto/auth_pb';
import { GrpcCallError } from '../utils/errors';
import { Permission, UserSession } from '../types/subject';
import { PermissionsRequest, PermissionsResponse } from '../proto/group_pb';
import { GroupClient } from '../proto/group_grpc_pb';
import { checkAndGetEnv } from '../utils';

const protoSessionToUserSession = (data: Session.AsObject): UserSession => ({
  id: data.id,
  userId: data.userId,
  username: data.username,
  fullName: data.fullName,
  validUntil: new Date(data.validUntil),
});

const protoPermissionsToUserPermissions = (data: PermissionsResponse.AsObject): Permission[] =>
  data.permissionsList.map((p) => ({ id: p.id, name: p.name } as Permission));

export const findSession = async (id: string): Promise<UserSession> => {
  const authClient = new AuthClient(checkAndGetEnv('GRPC_AUTH_CLIENT'), grpc.credentials.createInsecure());
  const request = new FindSessionRequest();
  request.setId(id);
  return new Promise((resolve, reject) => authClient.findSession(
      request,
      (err, resp) => err ? reject(new GrpcCallError(err)) : resolve(protoSessionToUserSession(resp.toObject()))
  ));
};

export const getPermissions = (groupId: string, userId: string): Promise<Permission[]> => {
  const groupClient = new GroupClient(checkAndGetEnv('GRPC_GROUP_CLIENT'), grpc.credentials.createInsecure());
  console.log(checkAndGetEnv('GRPC_GROUP_CLIENT'));
  const request = new PermissionsRequest();
  request.setGroupId(groupId);
  request.setUserId(userId);
  return new Promise((resolve, reject) => groupClient.getUserPermissions(
      request,
      (err, resp) => err ? reject(new GrpcCallError(err)) : resolve(protoPermissionsToUserPermissions(resp.toObject()))
  ));
};
