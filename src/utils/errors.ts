import * as grpc from 'grpc';

export class GrpcCallError extends Error {
  status: number;
  errorMessage: string;
  constructor(error: grpc.ServiceError) {
    super();
    const status = error.metadata?.get('status')[0];
    this.status = status ? +status : 500;
    this.errorMessage = error.details || '';
  }
}
