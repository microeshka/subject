// package: group
// file: group.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class PermissionsRequest extends jspb.Message { 
    getUserId(): string;
    setUserId(value: string): PermissionsRequest;
    getGroupId(): string;
    setGroupId(value: string): PermissionsRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PermissionsRequest.AsObject;
    static toObject(includeInstance: boolean, msg: PermissionsRequest): PermissionsRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PermissionsRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PermissionsRequest;
    static deserializeBinaryFromReader(message: PermissionsRequest, reader: jspb.BinaryReader): PermissionsRequest;
}

export namespace PermissionsRequest {
    export type AsObject = {
        userId: string,
        groupId: string,
    }
}

export class Permission extends jspb.Message { 
    getId(): string;
    setId(value: string): Permission;
    getName(): string;
    setName(value: string): Permission;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Permission.AsObject;
    static toObject(includeInstance: boolean, msg: Permission): Permission.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Permission, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Permission;
    static deserializeBinaryFromReader(message: Permission, reader: jspb.BinaryReader): Permission;
}

export namespace Permission {
    export type AsObject = {
        id: string,
        name: string,
    }
}

export class PermissionsResponse extends jspb.Message { 
    clearPermissionsList(): void;
    getPermissionsList(): Array<Permission>;
    setPermissionsList(value: Array<Permission>): PermissionsResponse;
    addPermissions(value?: Permission, index?: number): Permission;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PermissionsResponse.AsObject;
    static toObject(includeInstance: boolean, msg: PermissionsResponse): PermissionsResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PermissionsResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PermissionsResponse;
    static deserializeBinaryFromReader(message: PermissionsResponse, reader: jspb.BinaryReader): PermissionsResponse;
}

export namespace PermissionsResponse {
    export type AsObject = {
        permissionsList: Array<Permission.AsObject>,
    }
}
