import { Subject, SubjectUpdate, SubjectUpdate as SubjectShort } from '../types/subject';

export interface ISubjectRepository {
  save(data: Subject): Promise<void>;
  update(data: SubjectUpdate): Promise<Subject>;
  delete(id: string): Promise<void>;
  getById(id: string): Promise<Subject | undefined>;
  getSubjectsByGroupId(groupId: string): Promise<SubjectShort[]>;
}
