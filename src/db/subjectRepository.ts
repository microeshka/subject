import { Pool, PoolConfig } from 'pg';
import { Subject, SubjectUpdate, SubjectUpdate as SubjectShort } from '../types/subject';
import { ISubjectRepository } from './types';
import * as uuid from 'uuid';

export class SubjectRepository implements ISubjectRepository {
  private connectionPool: Pool;

  constructor(opts: PoolConfig) {
    try {
      this.connectionPool = new Pool(opts);
    } catch (e) {
      console.error('Failed to connect to Postgres: ', e);
      throw e;
    }
  }

  public async save(data: Subject): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query(
        'insert into subjects(id, title, description, group_id, creator_id, created_at) values ($1, $2, $3, $4, $5, $6)',
        [
          data.id,
          data.title,
          data.description,
          data.groupId,
          data.creatorId,
          data.createdAt,
        ]);
    client.release();
  }

  public async update(data: SubjectUpdate): Promise<Subject> {
    const client = await this.connectionPool.connect();
    await client.query('update subjects set title=$1, description=$2 WHERE id = $3', [
      data.title,
      data.description,
      data.id,
    ]);
    client.release();
    const subject = await this.getById(data.id);
    if (!subject) {
      throw { status: 404, errorMessage: 'Group not found!' };
    }
    return subject;
  }

  public async getById(id: string): Promise<Subject | undefined> {
    const client = await this.connectionPool.connect();
    const rawSubject = (await client.query('select * from subjects WHERE id=$1', [id])).rows[0];
    if (!rawSubject) return rawSubject;
    const subject = {
      id: rawSubject.id,
      title: rawSubject.title,
      description: rawSubject.description,
      createdAt: rawSubject['created_at'],
      creatorId: rawSubject['creator_id'],
      groupId: rawSubject['group_id'],
    } as Subject;
    client.release();
    return subject;
  }

  public async delete(id: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('delete from subjects WHERE id=$1', [id]);
    client.release();
  }

  public async getSubjectsByGroupId(groupId: string): Promise<SubjectShort[]> {
    const client = await this.connectionPool.connect();
    const rawSubjects = (await client.query('select * from subjects WHERE group_id=$1', [groupId])).rows;
    const subjects = rawSubjects.map((subject) => ({
      id: subject.id,
      title: subject.title,
      description: subject.description,
      groupId: subject['group_id'],
    } as SubjectShort));
    client.release();
    return subjects;
  }
}
