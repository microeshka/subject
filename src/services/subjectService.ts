import { ISubjectRepository } from '../db/types';
import { Subject, SubjectUpdate, SubjectUpdate as SubjectShort, SubjectCreate } from '../types/subject';
import * as uuid from 'uuid';

export interface ISubjectService {
  create(data: SubjectCreate): Promise<Subject>;
  update(data: SubjectUpdate): Promise<Subject>;
  delete(id: string): Promise<void>;
  getById(id: string): Promise<Subject>;
  getSubjectsByGroupId(groupId: string): Promise<SubjectShort[]>;
}

export class SubjectService implements ISubjectService {
  private subjectRepository: ISubjectRepository;

  constructor(subjectRepository: ISubjectRepository) {
    this.subjectRepository = subjectRepository;
  }

  public async create(data: SubjectCreate): Promise<Subject> {
    if (!data.creatorId) {
      throw { status: 500, errorMessage: 'Unable to create new subject. creatorId is not provided' };
    }
    const subject = {
      ...data,
      id: uuid.v4(),
      createdAt: new Date(),
    } as Subject;
    await this.subjectRepository.save(subject);
    return subject;
  }

  public async update(data: SubjectUpdate): Promise<Subject> {
    await this.getById(data.id);
    return this.subjectRepository.update(data);
  }

  public async delete(id: string): Promise<void> {
    await this.getById(id);
    return this.subjectRepository.delete(id);
  }

  public async getById(id: string): Promise<Subject> {
    const subject = await this.subjectRepository.getById(id);
    if (!subject) {
      throw { status: 404, errorMessage: `Subject with id=${id} not found!` };
    }
    return subject;
  }

  public async getSubjectsByGroupId(groupId: string): Promise<SubjectUpdate[]> {
    return this.getSubjectsByGroupId(groupId);
  }
}
